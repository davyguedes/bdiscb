create database bdiscb;
use bdiscb;
insert into Role(id, name, role) values(1, "Usuario", "ROLE_USER");
insert into Role(id, name, role) values(2, "Administrador", "ROLE_ADMIN");
insert into user(USER_ID, enabled, USERNAME, PASSWORD, role_id) values(1, TRUE, "dev", "e77989ed21758e78331b20e477fc5582", 1);
insert into user(USER_ID, enabled, USERNAME, PASSWORD, role_id) values(2, TRUE, "admin", "21232f297a57a5a743894a0e4a801fc3", 2);

