CREATE DATABASE  IF NOT EXISTS `bdiscb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `bdiscb`;
-- MySQL dump 10.13  Distrib 5.7.18, for Linux (x86_64)
--
-- Host: localhost    Database: bdiscb
-- ------------------------------------------------------
-- Server version	5.7.18-0ubuntu0.17.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `NumeroComiteEtica`
--

DROP TABLE IF EXISTS `NumeroComiteEtica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NumeroComiteEtica` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `statusTramitacao` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `NumeroComiteEtica`
--

LOCK TABLES `NumeroComiteEtica` WRITE;
/*!40000 ALTER TABLE `NumeroComiteEtica` DISABLE KEYS */;
/*!40000 ALTER TABLE `NumeroComiteEtica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `caixa`
--

DROP TABLE IF EXISTS `caixa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `caixa` (
  `CAIXA_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CAIXA_DESCRICAO` varchar(255) DEFAULT NULL,
  `CAIXA_ESPECIME_ESPECIE` varchar(60) DEFAULT NULL,
  `CAIXA_ESPECIME_TIPO` varchar(255) DEFAULT NULL,
  `CAIXA_ESPECIMES_FEMEA_QUANTIDADE` int(11) DEFAULT NULL,
  `CAIXA_ESPECIMES_MACHO_QUANTIDADE` int(11) DEFAULT NULL,
  `CAIXA_ESPECIME_RACA` varchar(60) DEFAULT NULL,
  `PROJETO_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`CAIXA_ID`),
  KEY `FK_5thbpixlt4vh5wld7qv9e22da` (`PROJETO_ID`),
  CONSTRAINT `FK_5thbpixlt4vh5wld7qv9e22da` FOREIGN KEY (`PROJETO_ID`) REFERENCES `projeto` (`PROJETO_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `caixa`
--

LOCK TABLES `caixa` WRITE;
/*!40000 ALTER TABLE `caixa` DISABLE KEYS */;
INSERT INTO `caixa` VALUES (3,'','','ADULTOS',0,6,'Wisstar',2),(4,'','','ADULTOS',0,6,'Wisstar',2),(5,'','','ADULTOS',3,0,'Wisstar',3),(6,'','','FILHOTES',14,0,'Wisstar',3),(7,'6 Ratos Wisstar fêmeas grávidas','','ADULTOS',6,0,'Wisstar',3),(8,'Ratos Wisstar machos adultos.','','ADULTOS',0,3,'Wisstar',3),(9,'Ratos Wisstar fêmeas adultas.','','ADULTOS',8,0,'Wisstar',3),(10,'Ratos Wisstar machos filhotes.','','FILHOTES',0,6,'Wisstar',3),(11,'Ratos Wisstar machos filhotes.','','ADULTOS',0,9,'Wisstar',3),(15,'sham - veiculo','rato','AMBOS',0,5,'wistar',5),(16,'','rato','ADULTOS',0,4,'wistar',5);
/*!40000 ALTER TABLE `caixa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projeto`
--

DROP TABLE IF EXISTS `projeto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projeto` (
  `PROJETO_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ALUNO_NOME` varchar(100) DEFAULT NULL,
  `PROJETO_DESCRICAO` varchar(255) DEFAULT NULL,
  `LABORATORIO` varchar(60) DEFAULT NULL,
  `PROJETO_NOME` varchar(100) DEFAULT NULL,
  `NUMERO_COMITE_ETICA` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PROJETO_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projeto`
--

LOCK TABLES `projeto` WRITE;
/*!40000 ALTER TABLE `projeto` DISABLE KEYS */;
INSERT INTO `projeto` VALUES (2,'Stela Mirla da Silva Felipe','Projeto\r\nTranscriptoma','LABIEX','Projeto Transcriptoma do Exercício Experimental','159206014'),(3,'Juliana Osório','Balanço redox no músculo esquelético pós indução por uma sessão de exercício extenuante: papel de NADPH Oxidase.','LABIEX','Balanço redox no músculo esquelético pós indução por uma sessão de exercício extenuante','5236655/2016'),(5,'Welton Daniel Nogueira Godinho','','LABIEX','Efeito da suplementação de melatonina em animais induzidos ao Alzheimer por b amyloide 1-42','123');
/*!40000 ALTER TABLE `projeto` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-22 23:04:27
