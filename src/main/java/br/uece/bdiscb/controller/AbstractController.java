package br.uece.bdiscb.controller;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.servlet.ModelAndView;

import br.uece.bdiscb.dao.Dao;
import br.uece.bdiscb.exception.CustomEntityNotFoundException;

@Transactional
public abstract class AbstractController<Entity> {

	@Autowired
	protected Dao<Entity> dao;

	protected abstract Entity getEntity();

	protected abstract String getPath(String page);

	protected int pageLength = 15;

	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
	}

	@ExceptionHandler(CustomEntityNotFoundException.class)
	public ModelAndView handleCustomException(CustomEntityNotFoundException ex) {
		ModelAndView mav = getModelAndView();
		mav.setViewName("error/404");
		mav.addObject("errCode", ex.getErrCode());
		mav.addObject("errMsg", ex.getErrMsg());
		return mav;
	}

	@ExceptionHandler(Exception.class)
	public ModelAndView handleAllException(Exception ex) {
		ModelAndView model = new ModelAndView("error/generic_error");
		model.addObject("errMsg", ex.getMessage());
		return model;
	}

	/*
	 * @RequestMapping(value = "", method = RequestMethod.GET) public
	 * ModelAndView listar(SessionStatus status) { List<Entity> entities =
	 * dao.list(); ModelAndView mav = getModelAndView();
	 * mav.addObject("entities", entities); mav.setViewName(getPath("listar"));
	 * status.setComplete(); return mav; }
	 * 
	 * @RequestMapping(value = "novo", method = RequestMethod.GET) public
	 * ModelAndView novo(HttpServletRequest request) { Entity entity =
	 * getEntity(); ModelAndView mav = getModelAndView();
	 * mav.addObject("entity", entity); mav.setViewName(getPath("novo")); return
	 * mav; }
	 * 
	 * @RequestMapping(value = "confirmar", params = "id", method =
	 * RequestMethod.GET) public ModelAndView confirmar(@RequestParam(value =
	 * "id") Long id) { ModelAndView mav = getModelAndView();
	 * mav.addObject("entity", getEntity());
	 * mav.setViewName(getPath("confirmar")); return mav; }
	 * 
	 * @RequestMapping(value = "confirmar", method = RequestMethod.POST) public
	 * ModelAndView confirmar(@ModelAttribute("entity") @Valid Entity entity,
	 * BindingResult result, HttpServletRequest request) { ModelAndView mav =
	 * getModelAndView(); if (result.hasErrors()) {
	 * mav.setViewName(getPath("novo")); return mav; } else {
	 * mav.addObject("entity", entity); mav.setViewName(getPath("confirmar"));
	 * return mav; } }
	 * 
	 * @RequestMapping(value = "salvar", method = RequestMethod.GET) public
	 * ModelAndView salvar(@ModelAttribute("entity") @Valid Entity entity,
	 * SessionStatus status) { dao.save(entity); status.setComplete();
	 * ModelAndView mav = getModelAndView(); mav.setViewName("redirect:/" +
	 * getPath("")); return mav; }
	 * 
	 * @RequestMapping(value = "editar", params = "id", method =
	 * RequestMethod.GET) public ModelAndView editar(@RequestParam(value = "id")
	 * Long id, Entity entity) { ModelAndView mav = getModelAndView(); entity =
	 * dao.find(id); // if (entity == null) // throw new
	 * CustomEntityNotFoundException(); mav.addObject("entity", entity);
	 * mav.setViewName(getPath("editar")); return mav; }
	 * 
	 * @RequestMapping(value = "atualizar", method = RequestMethod.POST) public
	 * ModelAndView atualizar(@ModelAttribute("entity") @Valid Entity entity,
	 * SessionStatus status) { dao.update(entity); status.setComplete(); return
	 * new ModelAndView("redirect:/" + getPath("")); }
	 * 
	 * 
	 * @RequestMapping(value = "detalhar", params = "id", method =
	 * RequestMethod.GET) public ModelAndView detalhar(@RequestParam(value =
	 * "id") Long id, Entity entity, ModelAndView mav) { entity = dao.find(id);
	 * if (entity == null) throw new CustomEntityNotFoundException();
	 * mav.addObject("entity", entity); mav.setViewName(getPath("detalhar"));
	 * return mav; }
	 * 
	 * @RequestMapping(value = "remover", params = "id", method =
	 * RequestMethod.GET) public ModelAndView remover(@RequestParam(value =
	 * "id") Long id) { Entity entity = dao.find(id); if (entity == null) throw
	 * new CustomEntityNotFoundException(); dao.delete(entity); return new
	 * ModelAndView("redirect:/" + getPath("")); }
	 * 
	 */ protected ModelAndView getModelAndView() {
		return new ModelAndView();
	}
}