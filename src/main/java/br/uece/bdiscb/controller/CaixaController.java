package br.uece.bdiscb.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import br.uece.bdiscb.dao.Dao;
import br.uece.bdiscb.exception.CustomEntityNotFoundException;
import br.uece.bdiscb.model.Caixa;
import br.uece.bdiscb.model.EspecimeTipo;
import br.uece.bdiscb.model.Projeto;

@Controller
@RequestMapping("/caixa")
@SessionAttributes("caixa")
public class CaixaController extends AbstractController<Caixa> {

	@Autowired
	protected Dao<Projeto> projetoDao;

	@Override
	protected Caixa getEntity() {
		return new Caixa();
	}

	@Override
	protected String getPath(String page) {
		return new StringBuilder("caixa/").append(page).toString();
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView listarPorProjeto(@RequestParam(value = "projetoId", required = false) Integer projetoId,
			SessionStatus status) {
		List<Caixa> entities;
		Projeto projeto = new Projeto();
		ModelAndView mav = getModelAndView();
		if (projetoId != null) {
			projeto = projetoDao.find(Long.valueOf(projetoId.toString()));
			if (projeto.getId() != null) {
				entities = dao.list(projetoId);
				projeto = projetoDao.find(Long.valueOf(projetoId.toString()));
				mav.addObject("projeto", projeto);
			} else
				throw new CustomEntityNotFoundException("404", "Recurso não encontrado.");
		} else
			throw new CustomEntityNotFoundException("404", "Recurso não encontrado.");
		entities = dao.list(projetoId);
		mav.addObject("entities", entities);
		mav.setViewName(getPath("listar"));
		status.setComplete();
		return mav;
	}

	@RequestMapping(value = "novo", method = RequestMethod.GET)
	public ModelAndView novo(@RequestParam(value = "projetoId", required = false) Integer projetoId,
			HttpServletRequest request) {
		Caixa caixa = getEntity();
		Projeto projeto = new Projeto();
		ModelAndView mav = getModelAndView();
		if (projetoId != null) {
			projeto = projetoDao.find(Long.valueOf(projetoId.toString()));
			if (projeto.getId() != null)
				caixa.setProjeto(projeto);
			else
				throw new CustomEntityNotFoundException("404", "Recurso não encontrado.");
		} else
			throw new CustomEntityNotFoundException("404", "Recurso não encontrado.");
		mav.addObject("tipos", EspecimeTipo.getAll());
		mav.addObject("projetoId", projetoId);
		mav.addObject("caixa", caixa);
		mav.setViewName(getPath("novo"));
		return mav;
	}

	@RequestMapping(value = "salvar", method = RequestMethod.POST)
	public ModelAndView confirmar(@RequestParam("projetoId") Integer projetoId,
			@ModelAttribute("caixa") @Valid Caixa caixa, BindingResult result, HttpServletRequest request,
			SessionStatus status) {
		ModelAndView mav = getModelAndView();
		if (result.hasErrors()) {
			mav.addObject("tipos", EspecimeTipo.getAll());
			mav.setViewName(getPath("novo"));
			mav.addObject("projetoId", projetoId);
			return mav;
		} else {
			if (caixa.getId() == 0)
				caixa.setId(null);
			dao.save(caixa.getProjeto().getId(), caixa);
			status.setComplete();
			mav.setViewName("redirect:/" + getPath("?projetoId=" + caixa.getProjeto().getId()));
			return mav;
		}
	}

	@RequestMapping(value = "salvar", method = RequestMethod.GET)
	public ModelAndView salvar(@ModelAttribute("caixa") @Valid Caixa caixa, SessionStatus status) {
		if (caixa.getId() == 0)
			caixa.setId(null);
		dao.save(caixa.getProjeto().getId(), caixa);
		status.setComplete();
		ModelAndView mav = getModelAndView();
		mav.setViewName("redirect:/" + getPath("?projetoId=" + caixa.getProjeto().getId()));
		return mav;
	}

	@RequestMapping(value = "editar", params = "id", method = RequestMethod.GET)
	public ModelAndView editar(@RequestParam(value = "id") Long id, Caixa caixa) {
		ModelAndView mav = getModelAndView();
		caixa = dao.find(id);
		mav.addObject("projetoId", caixa.getProjeto().getId());
		mav.addObject("tipos", EspecimeTipo.getAll());
		mav.addObject("caixa", caixa);
		mav.setViewName(getPath("editar"));
		return mav;
	}

	@RequestMapping(value = "atualizar", method = RequestMethod.POST)
	public ModelAndView atualizar(@RequestParam("projetoId") Integer projetoId,
			@ModelAttribute("caixa") @Valid Caixa caixa, BindingResult result, HttpServletRequest request,
			SessionStatus status) {
		ModelAndView mav = getModelAndView();
		if (result.hasErrors()) {
			mav.setViewName(getPath("novo"));
			mav.addObject("projetoId", projetoId);
			return mav;
		} else {
			if (caixa.getId() == 0)
				caixa.setId(null);
			dao.update(caixa);
			status.setComplete();
			mav.setViewName("redirect:/" + getPath("?projetoId=" + caixa.getProjeto().getId()));
			return mav;
		}
	}

	@RequestMapping(value = "detalhar", params = "id", method = RequestMethod.GET)
	public ModelAndView detalhar(@RequestParam(value = "id") Long id, Caixa caixa, ModelAndView mav) {
		caixa = dao.find(id);
		if (caixa == null)
			throw new CustomEntityNotFoundException("404", "Recurso não encontrado.");
		mav.addObject("caixa", caixa);
		mav.setViewName(getPath("detalhar"));
		return mav;
	}

	@RequestMapping(value = "remover", params = "id", method = RequestMethod.GET)
	public ModelAndView remover(@RequestParam(value = "id") Long id) {
		Caixa caixa = dao.find(id);
		if (caixa == null)
			throw new CustomEntityNotFoundException("404", "Recurso não encontrado.");
		dao.delete(caixa);
		return new ModelAndView("redirect:/" + getPath("?projetoId=" + caixa.getProjeto().getId()));
	}
}
