package br.uece.bdiscb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/*@Controller
@RequestMapping("/")
public class IndexController {
	@RequestMapping(value = {"", "index"}, method = RequestMethod.GET)
	public ModelAndView welcomePage(){
		return new ModelAndView("redirect:projeto");
	};
	
	
}*/

@Controller
@RequestMapping("/")
public class IndexController {
	
	@RequestMapping(value = {"", "index"}, method = RequestMethod.GET)
	public ModelAndView welcomePage(){
		return new ModelAndView("redirect:projeto");
	};
	
	@RequestMapping(value = "login", method = RequestMethod.GET)
	public ModelAndView login(
			@RequestParam(value = "login_error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout){
		ModelAndView model = new ModelAndView();
		if(error != null) {
			model.addObject("error", "Usuário e senha sem correspondência.");
		}
		
		if(logout != null){
			model.addObject("msg", "Você fez logout com sucesso!");
		}
		model.setViewName("/login");
		return model;
	}
}

