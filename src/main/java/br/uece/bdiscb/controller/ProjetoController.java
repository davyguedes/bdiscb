package br.uece.bdiscb.controller;

import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import br.uece.bdiscb.model.Projeto;

@Controller
@RequestMapping("/projeto")
@SessionAttributes("projeto")
public class ProjetoController extends AbstractController<Projeto>{
	@Override
	protected Projeto getEntity() {
		return new Projeto();
	}

	@Override
	protected String getPath(String page) {
			return new StringBuilder("projeto/").append(page).toString();
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public ModelAndView projetoById(@PathVariable("id")Long id){
		ModelAndView mav = getModelAndView();
		Projeto projeto = dao.find(id);
		mav.addObject("projeto", projeto);
		return mav;
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView listar(SessionStatus status) {
		List<Projeto> entities = dao.listAll();
		ModelAndView mav = getModelAndView();
		mav.addObject("entities", entities);
		mav.setViewName(getPath("listar"));
		status.setComplete();
		return mav;
	}

	@RequestMapping(value = "novo", method = RequestMethod.GET)
	public ModelAndView novo(HttpServletRequest request) {
		Projeto projeto = getEntity();
		ModelAndView mav = getModelAndView();
		mav.addObject("projeto", projeto);
		mav.setViewName(getPath("novo"));
		return mav;
	}

	@RequestMapping(value = "confirmar", params = "id", method = RequestMethod.GET)
	public ModelAndView confirmar(@RequestParam(value = "id") Long id) {
		ModelAndView mav = getModelAndView();
		mav.addObject("projeto", getEntity());
		mav.setViewName(getPath("confirmar"));
		return mav;
	}

	@RequestMapping(value = "confirmar", method = RequestMethod.POST)
	public ModelAndView confirmar(@ModelAttribute("projeto") @Valid Projeto projeto, BindingResult result,
			HttpServletRequest request) {
		ModelAndView mav = getModelAndView();
		if (result.hasErrors()) {
			mav.setViewName(getPath("novo"));
			return mav;
		} else {
			mav.addObject("projeto", projeto);
			mav.setViewName(getPath("confirmar"));
			return mav;
		}
	}

	@RequestMapping(value = "salvar", method = RequestMethod.GET)
	public ModelAndView salvar(@ModelAttribute("projeto") @Valid Projeto projeto, SessionStatus status) {
		dao.save(projeto);
		status.setComplete();
		ModelAndView mav = getModelAndView();
		mav.setViewName("redirect:/" + getPath(""));
		return mav;
	}

	@RequestMapping(value = "editar", params = "id", method = RequestMethod.GET)
	public ModelAndView editar(@RequestParam(value = "id") Long id, Projeto projeto) {
		ModelAndView mav = getModelAndView();
		projeto = dao.find(id);
//		if (projeto == null)
//			throw new CustomEntityNotFoundException();
		mav.addObject("projeto", projeto);
		mav.setViewName(getPath("editar"));
		return mav;
	}

	@RequestMapping(value = "atualizar", method = RequestMethod.POST)
	public ModelAndView atualizar(@ModelAttribute("projeto") @Valid Projeto projeto, SessionStatus status) {
		dao.update(projeto);
		status.setComplete();
		return new ModelAndView("redirect:/" + getPath(""));
	}


	@RequestMapping(value = "detalhar", params = "id", method = RequestMethod.GET)
	public ModelAndView detalhar(@RequestParam(value = "id") Long id, Projeto projeto, ModelAndView mav) {
		projeto = dao.find(id);
		if (projeto == null)
			throw new EntityNotFoundException();
		mav.addObject("projeto", projeto);
		mav.setViewName(getPath("detalhar"));
		return mav;
	}

	@RequestMapping(value = "remover", params = "id", method = RequestMethod.GET)
	public ModelAndView remover(@RequestParam(value = "id") Long id) {
		Projeto projeto = dao.find(id);
		if (projeto == null)
			throw new EntityNotFoundException();
		dao.delete(projeto);
		return new ModelAndView("redirect:/" + getPath(""));
	}
	
}
