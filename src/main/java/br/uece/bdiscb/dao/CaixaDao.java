package br.uece.bdiscb.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

import org.springframework.stereotype.Component;

import br.uece.bdiscb.model.Caixa;
import br.uece.bdiscb.model.Projeto;

@Component
public class CaixaDao implements Dao<Caixa> {
	@PersistenceContext
	private EntityManager em;
	/*
	 * @Autowired private EspecimeDao amostraDao;
	 */

	public List<Caixa> list(Integer projetoId) {
		Projeto projeto;
		try {
			projeto = (Projeto) em
					.createQuery("select p from Projeto p left join fetch p.caixas where p.id=" + projetoId)
					.getSingleResult();
			return new ArrayList<Caixa>(projeto.getCaixas());
		} catch (NoResultException e) {
			return new ArrayList<Caixa>();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Caixa> listAll() {
		List<Caixa> caixas = (List<Caixa>) em.createQuery("select c from Caixa c").getResultList();
		return caixas;
	}

	public Caixa find(Long id) {
		Caixa caixa;
		try {
			caixa = (Caixa) em.createQuery("select c from Caixa c where c.id=" + id).getSingleResult();
		} catch (NoResultException e) {
			caixa = new Caixa();
		}
		return caixa;
	}

	@Override
	public Boolean save(Caixa entity) {
		try {
			throw new Exception("Método CaixaDao.save necessita do parametro projetoId");
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	@Override
	public Boolean save(Long projetoId, Caixa caixa) throws ConstraintViolationException {
		try {
			Projeto projeto;
			// verificar a troca para SAVE
			em.merge(caixa);
			projeto = (Projeto) em
					.createQuery("select p from Projeto p left join fetch p.caixas where p.id=" + projetoId)
					.getSingleResult();
			projeto.getCaixas().add(caixa);
			em.merge(projeto);
			return true;
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		/*
		 * try { em.merge(caixa); } catch (ConstraintViolationException e) {
		 * e.printStackTrace(); throw e; } catch (Exception e) {
		 * e.printStackTrace(); return false; } return true;
		 */
	}

	public void delete(Caixa caixa) {
		caixa = (Caixa) em.find(Caixa.class,
				em.getEntityManagerFactory().getPersistenceUnitUtil().getIdentifier(caixa));
		/* List<Especime> cobaias = caixa.getCobaias(); */
		em.remove(caixa);

		/*
		 * for (Especime cobaia : cobaias) { amostraDao.delete(cobaia); }
		 */
	}

	public Boolean update(Caixa caixa) {
		em.merge(caixa);
		return true;
	}

}