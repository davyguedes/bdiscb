package br.uece.bdiscb.dao;
import java.util.List;

import javax.validation.ConstraintViolationException;

public interface Dao<Entity> {
	List<Entity> listAll();
	List<Entity> list(Integer id);
	Entity find(Long id);
	Boolean save(Entity entity);
	Boolean save(Long id, Entity entity) throws ConstraintViolationException; 
	Boolean update(Entity entity);
	void delete(Entity entity);
}
