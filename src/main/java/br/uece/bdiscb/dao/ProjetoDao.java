package br.uece.bdiscb.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.uece.bdiscb.model.Caixa;
import br.uece.bdiscb.model.Projeto;

@Component
public class ProjetoDao implements Dao<Projeto>{
	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	private CaixaDao caixaDao;
	
	@SuppressWarnings("unchecked")
	public List<Projeto> listAll() {
		List<Projeto> projetos = (List<Projeto>) em.createQuery("select e from Projeto e")
				.getResultList();
		return projetos;
	}
	
	@SuppressWarnings("unchecked")
	public List<Projeto> list(Integer id) {
		List<Projeto> projetos = (List<Projeto>) em.createQuery("select e from Projeto e")
				.getResultList();
		return projetos;
	}
	
	public Projeto find(Long id) {
		Projeto projeto;
		try {
			projeto = (Projeto) em.createQuery("select e from Projeto e left join fetch e.caixas where e.id=" + id)
					.getSingleResult();
		} catch (NoResultException e) {
			projeto = new Projeto();
		}
		return projeto;
	}
	
	public Boolean save(Projeto projeto) throws ConstraintViolationException{
		try {
			em.merge(projeto);
		} 
		catch (ConstraintViolationException e) {
			e.printStackTrace();
			throw  e;
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public Boolean save(Long id, Projeto projeto) throws ConstraintViolationException{
		try {
			em.merge(projeto);
		} 
		catch (ConstraintViolationException e) {
			e.printStackTrace();
			throw  e;
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public void delete(Projeto projeto) {
		projeto = (Projeto) em.find(Projeto.class, em.getEntityManagerFactory().getPersistenceUnitUtil().getIdentifier(projeto));
		List<Caixa> caixas = projeto.getCaixas();
		for (Caixa caixa: caixas) {
			caixaDao.delete(caixa);
		}
		em.remove(projeto);
		   
	}
	
	public Boolean update(Projeto projeto) {
		em.merge(projeto);
		return true;
	}

}
