package br.uece.bdiscb.dao;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

import br.uece.bdiscb.model.Role;

public class RoleDao {
	@PersistenceContext
	protected EntityManager em;

	@SuppressWarnings("unchecked")
	public List<Entity> list() {
		List<Entity> entities = (List<Entity>) em.createQuery("select e from Role e")
				.getResultList();
		return entities;
	}
	
	public Entity find(Long id) {
		Entity entity;
		try {
			entity = (Entity) em.createQuery("select e from Role e where e.id=" + id)
					.getSingleResult();
		} catch (NoResultException e) {
			entity = null;
		}
		return entity;
	}

	
	public Boolean save(Entity entity) throws ConstraintViolationException{
		try {
			em.merge(entity);
		} 
		catch (ConstraintViolationException e) {
			e.printStackTrace();
			throw  e;
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public Boolean update(Entity entity) {
		em.merge(entity);
		return true;
	}

	public Boolean delete(Entity entity) {
		entity = (Entity) em.find(Role.class, em.getEntityManagerFactory().getPersistenceUnitUtil().getIdentifier(entity));
		em.remove(entity);
		return true;
	}
}
