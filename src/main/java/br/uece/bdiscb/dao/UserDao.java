package br.uece.bdiscb.dao;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

import org.springframework.stereotype.Repository;

import br.uece.bdiscb.model.User;



@Repository
public class UserDao{

	@PersistenceContext
	protected EntityManager em;

	
	public int numberRecords() {
		int numberRecords = em.createQuery("select e from user e").getResultList().size();
		return numberRecords;
	}
	public User find(Long id) {
		User user;
		try {
			user = (User) em.createQuery("select e from user e where e.id=" + id)
					.getSingleResult();
		} catch (NoResultException e) {
			user = null;
		}
		return user;
	}

	
	public Boolean save(User user) throws ConstraintViolationException{
		try {
			em.merge(user);
		} 
		catch (ConstraintViolationException e) {
			e.printStackTrace();
			throw  e;
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	


	public Boolean update(User user) {
		em.merge(user);
		return true;
	}

	
	public Boolean delete(User user) {
		user = (User) em.find(User.class, em.getEntityManagerFactory().getPersistenceUnitUtil().getIdentifier(user));
		em.remove(user);
		return true;
	}

	@SuppressWarnings("unchecked")
	
	public List<User> list() {
		List<User> entities = (List<User>) em.createQuery("select e from user e")
				.getResultList();
		return entities;
	}

	
	public User loadUserByUsername(final String username) throws NoResultException {
		StringBuilder jpql = new StringBuilder("select e from User e where e.username = :username");
		return (User) em.createQuery(jpql.toString()).setParameter("username", username).getSingleResult();
	}
}
