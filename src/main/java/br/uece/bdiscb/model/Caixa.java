package br.uece.bdiscb.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;

@Entity
@Table(name = "caixa", uniqueConstraints = { @UniqueConstraint(columnNames = "CAIXA_ID") })
public class Caixa implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "CAIXA_ID")
	private Long id;

	@Size(min = 0, max = 255)
	@Column(name = "CAIXA_DESCRICAO")
	private String descricao;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PROJETO_ID", nullable = false)
	private Projeto projeto = new Projeto();

	/*@OneToMany(fetch = FetchType.LAZY, mappedBy = "caixa")
	private List<Especime> especimes;*/
	
	@Size(max = 60, message = "60 caracteres é o máximo permitido.")
	@Column(name = "CAIXA_ESPECIME_ESPECIE")
	private String especie;

	@Size(max = 60, message = "60 caracteres é o máximo permitido")
	@Column(name = "CAIXA_ESPECIME_RACA")
	private String raca;

	@Column(name = "CAIXA_ESPECIMES_MACHO_QUANTIDADE")
	private Integer especimesMacho;

	@Column(name = "CAIXA_ESPECIMES_FEMEA_QUANTIDADE")
	private Integer especimesFemea;

	@Enumerated(EnumType.STRING)
	@Column(name="CAIXA_ESPECIME_TIPO")
	private EspecimeTipo especimeTipo;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setEspecimes(Integer especimes) {
	}

	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getEspecie() {
		return especie;
	}

	public void setEspecie(String especie) {
		this.especie = especie;
	}

	public String getRaca() {
		return raca;
	}

	public void setRaca(String raca) {
		this.raca = raca;
	}

	public Integer getEspecimesMacho() {
		return especimesMacho;
	}

	public void setEspecimesMacho(Integer especimesMacho) {
		this.especimesMacho = especimesMacho;
	}

	public Integer getEspecimesFemea() {
		return especimesFemea;
	}

	public void setEspecimesFemea(Integer especimesFemea) {
		this.especimesFemea = especimesFemea;
	}

	public EspecimeTipo getEspecimeTipo() {
		return especimeTipo;
	}

	public void setEspecimeTipo(EspecimeTipo especimeTipo) {
		this.especimeTipo = especimeTipo;
	}

}
