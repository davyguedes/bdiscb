package br.uece.bdiscb.model;

import java.util.ArrayList;
import java.util.List;

public enum EspecimeTipo {
	ADULTOS("A"),
	FILHOTES("F"),
	AMBOS("B");
	
	private String codigo;

	EspecimeTipo(String codigo) {
		this.codigo = codigo;
	}
	
	
	public String getCodigo() {
		return codigo;
	}


	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	public static List<EspecimeTipo> getAll(){
		List<EspecimeTipo> tipos = new ArrayList<EspecimeTipo>();
		tipos.add(EspecimeTipo.ADULTOS);
		tipos.add(EspecimeTipo.FILHOTES);
		tipos.add(EspecimeTipo.AMBOS);
		return tipos;
	}
	
	public static EspecimeTipo getFromCodigo(String codigo) {
	    if (codigo != null) {
	      for (EspecimeTipo a : EspecimeTipo.values()) {
	        if (codigo.equalsIgnoreCase(a.codigo)) {
	          return a;
	        }
	      }
	    }
	    return null;
	  }	
}
