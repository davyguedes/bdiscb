package br.uece.bdiscb.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="projeto", uniqueConstraints={
		@UniqueConstraint(columnNames="PROJETO_ID")
})
public class Projeto {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="PROJETO_ID")
	@NotNull
	private Long id;

	@Size(min = 1, max = 60)
	@Column(name="LABORATORIO")
	private String laboratorio;

	@Size(min = 1, max = 100)
	@Column(name="ALUNO_NOME")
	private String aluno;

	@Size(min=1)
	@Column(name="NUMERO_COMITE_ETICA")
	private String numeroComiteEtica;
	
	@Size(min = 1, max = 100)
	@Column(name="PROJETO_NOME")
	private String nomeProjeto;

	@Size(max = 255, min = 0)
	@Column(name="PROJETO_DESCRICAO")
	private String descricao;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "projeto")
	private List<Caixa> caixas;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomeProjeto() {
		return nomeProjeto;
	}

	public void setNomeProjeto(String nome) {
		this.nomeProjeto = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Caixa> getCaixas() {
		return caixas;
	}

	public void setCaixas(List<Caixa> caixas) {
		this.caixas = caixas;
	}

	public String getLaboratorio() {
		return laboratorio;
	}

	public void setLaboratorio(String laboratorio) {
		this.laboratorio = laboratorio;
	}

	public String getAluno() {
		return aluno;
	}

	public void setAluno(String aluno) {
		this.aluno = aluno;
	}

	public String getNumeroComiteEtica() {
		return numeroComiteEtica;
	}

	public void setNumeroComiteEtica(String numeroComiteEtica) {
		this.numeroComiteEtica = numeroComiteEtica;
	}

	// @OneToOne
	// private NumeroComiteEtica numeroComiteEtica;

}
