package br.uece.bdiscb.model;

import java.util.ArrayList;
import java.util.List;

public enum StatusTramitacao {
	APROVADO("Aprovado"), 
	RECUSADO("Recusado"),
	TRAMITE("Em trâmitação");
	
	private String status;
	
	
	StatusTramitacao(String status){
		this.setStatus(status);
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}
	
	public static List<StatusTramitacao> getstatusList() {
		List<StatusTramitacao> statusList = new ArrayList<StatusTramitacao>();
		statusList.add(StatusTramitacao.TRAMITE);
		statusList.add(StatusTramitacao.APROVADO);
		statusList.add(StatusTramitacao.RECUSADO);
		return statusList;
	}
	
	public static StatusTramitacao getStatus(String status) {
	    if (status != null) {
	      for (StatusTramitacao a : StatusTramitacao.values()) {
	        if (status.equalsIgnoreCase(a.status)) {
	          return a;
	        }
	      }
	    }
	    return null;
	  }
}
