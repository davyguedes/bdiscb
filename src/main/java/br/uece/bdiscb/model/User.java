package br.uece.bdiscb.model;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;


@Entity
@Table(name="user", uniqueConstraints={
		@UniqueConstraint(columnNames="USER_ID")
})
public class User implements UserDetails {
	/**
	 * 
	 */
	private static final long serialVersionUID = -356096984665019713L;

	@Id
	@GeneratedValue
	@Column(name="USER_ID")
	@NotNull
	private int id;

	@NotNull(message = "O campo \"Usuário\" não pode ser nulo")
	@Size(min = 4, max = 20, message = "O campo \"Usuário\" deve possuir entre 4 e 20 caracteres")
	@Column(name="USERNAME")
	private String username;

	@NotNull(message = "O campo \"Senha\" não pode ser nulo")
	@Size(min = 4, max = 100, message = "O campo \"Senha\" deve possuir entre 4 e 20 caracteres")
	@Column(name="PASSWORD")
	private String password;

	@OneToOne
	private Role role;

	private boolean enabled = true;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		if(password.length()>4 &&  password.length()<20)
			this.password = generateMD5(password);
		else this.password = password; 
	}

	private String generateMD5(String password) {
		MessageDigest messageDigest;
		try {
			messageDigest = MessageDigest.getInstance("MD5");
			messageDigest.update(password.getBytes(), 0, password.length());
			return new BigInteger(1, messageDigest.digest()).toString(16);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<Role> roles = new ArrayList<>();
		roles.add(this.role);
		return roles;
	}

	@Override
	public boolean isAccountNonExpired() {
		return false;
	}

	@Override
	public boolean isAccountNonLocked() {
		return false;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return false;
	}

}
