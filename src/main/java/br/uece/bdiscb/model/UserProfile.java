package br.uece.bdiscb.model;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserProfile {
	@Size(min=2, max=255, message="O campo necessita pelo menos de 2 caracteres.")
	private String nome;
	@Size(min = 10, max = 11, message = "O campo \"Telefone\" não pode ser nulo e deve ser válido")
	private String telefone;
	@Size(min = 1, max = 100)
	@Pattern(regexp = "^[\\w\\-]+(\\.[\\w\\-]+)*@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$", message = "E-mail com formato incorreto.")
	private String email;
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}