package br.uece.bdiscb.security;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import br.uece.bdiscb.model.User;



@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private UserDetailsService userService;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String username = authentication.getName();
		String password = (String) authentication.getCredentials();
		try {
			MessageDigest messageDigest;
			messageDigest = MessageDigest.getInstance("MD5");
			messageDigest.update(password.getBytes(), 0, password.length());
			password = new BigInteger(1, messageDigest.digest()).toString(16);
			User user;
			try{
				user = (User) userService.loadUserByUsername(username);
			} catch (NoResultException e1){
				user = null;
			}

			if (user == null) {
				throw new BadCredentialsException("Usuário não encontrado.");
			}

			if (!password.equals(user.getPassword())) {
				throw new BadCredentialsException("Senha incorreta.");
			}

			Collection<? extends GrantedAuthority> authorities = user.getAuthorities();

			return new UsernamePasswordAuthenticationToken(user, password, authorities);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean supports(Class<?> arg0) {
		return true;
	}

	public static void main(String[] args) throws NoSuchAlgorithmException {
		String s = "renata";

		MessageDigest m = MessageDigest.getInstance("MD5");

		m.update(s.getBytes(), 0, s.length());

		System.out.println(new BigInteger(1, m.digest()).toString(16));
	}
}
