<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="decorator"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

<link rel='stylesheet prefetch'
	href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'>
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/resources/css/mobile.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/resources/css/bootstrap.min.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/resources/font-awesome/css/font-awesome.min.css">
<script
	src="${pageContext.request.contextPath }/resources/js/jquery.min.js"></script>
<script
	src="${pageContext.request.contextPath }/resources/js/bootstrap.min.js"></script>
<title></title>
<style>
.center {
	text-align: center;
}

.fa {
	margin-right: 0px;
	margin-left: 0px;
}

table {
	border-collapse: collapse;
	border-spacing: 0;
	width: 100%;
	border: 1px solid #ddd;
}

th, td {
	border: none;
	text-align: left;
	padding: 8px;
/* 	padding-top: 8px; */
/*     padding-right: 0px; */
/*     padding-bottom: 8px; */
/*     padding-left: 0px; */
}

hr {
	border-top: 0px solid #fdf6e3;
}

.btn {
	border-radius: 10px;
}
</style>
</head>
<body>
	<div class="bs-component">
		<nav class="navbar navbar-default">
		<div class="container-fluid">
			<c:url value="/logout" var="logoutUrl" />
			<form id="logout" action="${logoutUrl}" method="post">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
			</form>
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand"
					href="${pageContext.request.contextPath }/projeto/">Banco de
					Dados ISCB/UECE</a>
			</div>

			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<!-- <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li> -->
					<li><a href="${pageContext.request.contextPath }/projeto/">Inicial</a></li>
					<li><a href="#">Sobre</a></li>
					<li><a href="#">Contato</a></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-expanded="false">Outros
							sites <span class="caret"></span>
					</a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="http://www.uece.br/uece/" target="_blank">UECE</a></li>
							<li class="divider"></li>
							<li><a href="http://www.labiexbioinformatics.com/"
								target="_blank">Labiex</a></li>
						</ul></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					 <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">${pageContext.request.userPrincipal.name}<b class="caret"></b></a>
           				 <ul class="dropdown-menu">
							<%-- <li><a
								href="${pageContext.request.contextPath }/usuario/perfil"><span
									class="fa faBar fa-user"></span>&ensp;Perfil</a></li> --%>
							<li><a
								href="javascript:document.getElementById('logout').submit()"><span
									class="fa faBar fa-sign-out"></span>&ensp;Logout</a></li>
							<li class="divider"></li>
						</ul></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#">Contato</a></li>
				</ul>
			</div>
		</div>
		</nav>
		<div id="source-button" class="btn btn-primary btn-xs"
			style="display: none;">&lt; &gt;</div>
	</div>
	<div id="content">
		<decorator:body />
	</div>
	<hr>
	<footer class="footer" style="bottom: 0px">
	<div class="container">
		<p>UECE - Universidade Estadual do Ceará</p>
		<p>
			<a href="uece.br" target="_blank">Site UECE</a>
		</p>
		<p>
			Sistema desenvolvido pela equipe do <strong>LABIEX</strong>
		</p>
	</div>
	</footer>
	<script type="text/javascript">
		$('.navbar .dropdown').hover(
				function() {
					$(this).find('.dropdown-menu').first().stop(true, true)
							.delay(0).slideDown();
				},
				function() {
					$(this).find('.dropdown-menu').first().stop(true, true)
							.delay(200).slideUp()
				});
	</script>
</body>
</html>
