
	
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/resources/css/bootstrap.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Acesso negado!</title>
</head>
<body>
	<div class="container">
		<h2 style="color: #e92f32">Acesso negado!</h2>
		<h4 style="color: #e92f32">${msg}</h4>
	</div>
</body>
</html>