

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet"
	href="${pageContext.request.contextPath }/resources/css/solar.min.css">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Projetos</title>
</head>
<body>
	<div class="container">
		<h2>Confirmação de caixa</h2>
		<form class="form-horizontal"
			action="${pageContext.request.contextPath }/caixa/salvar?projetoId=${projeto.id}"
			method="get">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>Campo</th>
						<th>Valor</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Quantidade de espécimes</td>
						<td>${caixa.especimes}</td>
					</tr>
					<tr>
						<td>Porções de ração</td>
						<td>${caixa.racaoPorcoes}</td>
					</tr>
					<tr>
						<td>Descrição da caixa</td>
						<td>${caixa.descricao}</td>
					</tr>
				</tbody>
			</table>
			<div class="container">
				<div class="form-group">

					<button class="btn btn-warning" type="button" name="back"
						onclick="history.back()">Corrigir</button>
					<button type="submit" class="btn btn-success">Confirmar</button>
				</div>
			</div>
		</form>
	</div>
</body>
</html>