<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<link rel="stylesheet"
	href="${pageContext.request.contextPath }/resources/css/bootstrap.min.css">
<%-- <%@ taglib prefix="spring-form" uri="/WEB-INF/tld/spring-form.tld"%> --%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Projetos</title>
</head>
<body>
	<div class="container">
		<h2>Caixa</h2>
		<form class="form-horizontal"
			action="${pageContext.request.contextPath }/caixa/" method="get">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>Campo</th>
						<th>Valor</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Espécie</td>
						<td>${caixa.especie }</td>
					</tr>
					<tr>
						<td>Raça</td>
						<td>${caixa.raca }</td>
					</tr>
					<tr>
						<td>Total na caixa</td>
						<td>${caixa.especimesMacho + caixa.especimesFemea}</td>
					</tr>
					<tr>
						<td>Machos</td>
						<td>${caixa.especimesMacho }</td>
					</tr>
					<tr>
						<td>Fêmeas</td>
						<td>${caixa.especimesFemea }</td>
					</tr>
					<tr>
						<td>Tipo</td>
						<td>${caixa.especimeTipo}</td>
					</tr>
					<tr>
						<td>Descrição da caixa</td>
						<td>${caixa.descricao}</td>
					</tr>
				</tbody>
			</table>

			<div class="container">
				<div class="form-group">

					<button class="btn btn-default" type="button" name="back"
						onclick="history.back()">Voltar</button>
					<a class="btn btn-primary"
						href="${pageContext.request.contextPath }/caixa/editar?id=${caixa.id}">Editar</a>
					<a class="btn btn-warning"
						href="${pageContext.request.contextPath }/caixa/remover?id=${caixa.id}">Remover</a>
				</div>
			</div>
		</form>
	</div>
</body>
</html>