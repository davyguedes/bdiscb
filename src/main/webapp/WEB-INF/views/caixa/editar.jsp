

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/resources/css/solar.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Caixas</title>
</head>
<body>
	<div class="container">
		<div class="well bs-component">
			<form:form cssClass="form-horizontal"
				action="${pageContext.request.contextPath }/caixa/atualizar?projetoId=${projetoId }"
				method="post" modelAttribute="caixa">
				<fieldset>
					<legend>Caixa</legend>
					<legend></legend>
					<form:errors path="*" cssClass="alert alert-danger" element="div" />
					<input type="hidden" id="id" name="id" value="${caixa.id }"
						readonly class="form-control">
					<div class="form-group">
						<label class="col-lg-2 control-label">Espécie</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" name="especie"
								value="${caixa.especie }" placeholder="espécie (opcional)">
						</div>
					</div>

					<div class="form-group">
						<label class="col-lg-2 control-label">Raça</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" name="raca"
								value="${caixa.raca}" placeholder="raça">
						</div>
					</div>

					<div class="form-group">
						<label class="col-lg-2 control-label">Número de espécimes
							MACHOS</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" name="especimesMacho"
								id="macho" onkeyup="sum();" value="${caixa.especimesMacho }"
								placeholder="porções de ração na caixa destinadas às espécimes">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">Número de espécimes
							FÊMEAS</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" name="especimesFemea"
								id="femea" onkeyup="sum();" value="${caixa.especimesFemea }"
								placeholder="porções de ração na caixa destinadas às espécimes">
						</div>
					</div>

					<div class="form-group">
						<label for="select" class="col-lg-2 control-label">Tipo</label>
						<div class="col-lg-10">
							<select name="especimeTipo" class="form-control">
								<option value="${caixa.especimeTipo}">
									<c:out
										value="${caixa.especimeTipo.codigo} : ${caixa.especimeTipo}" />
								</option>
								<c:forEach items="${tipos}" var="tipos">
									<option value="${tipos}">
										<c:out value="${tipos.codigo} : ${tipos}"></c:out>
									</option>
								</c:forEach>
							</select>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-2 control-label">Descrição da caixa</label>
						<div class="col-lg-10">
							<textarea class="form-control" rows="3" name="descricao"
								placeholder="descrição...">${caixa.descricao}</textarea>
							<span class="help-block">Uma pequena descrição quanto às
								espécimes presentes na caixa (Espécie/Raça/Sexo) e algumas
								observações</span>
						</div>
					
		</div>
		<div class="form-group">
			<div class="col-lg-10 col-lg-offset-2">
				<button type="button" name="back" onclick="history.back()"
					class="btn btn-warning">Cancelar</button>
				<button type="submit" class="btn btn-success">Atualizar</button>
			</div>
		</div>
		</fieldset>
		</form:form>
	</div>
	</div>
</body>
</html>