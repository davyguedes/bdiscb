

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/resources/css/solar.min.css">
<link href="${pageContext.request.contextPath }/resources/css/mfb.css"
	rel="stylesheet">
<script src="${pageContext.request.contextPath }/resources/js/mfb.js"></script>
<title>Caixas</title>
</head>
<body>
	<div class="container">
		<div class="">
			<div class="form-horizontal">
				<legend>Projeto</legend>
				<div class="col-md-6">
					<dl class="dl-horizontal">
						<dt>Nome do Laboratório</dt>
						<dd>${projeto.laboratorio }</dd>
						<dt>Nome do aluno</dt>
						<dd>${projeto.aluno }</dd>
						<dt>Comitê de Ética</dt>
						<dd>${projeto.numeroComiteEtica }</dd>
					</dl>
				</div>
				<div class="col-md-6">
					<dl class="dl-horizontal">
						<dt>Nome do Projeto</dt>
						<dd>${projeto.nomeProjeto}</dd>
						<dt>Descrição</dt>
						<dd>${projeto.descricao }</dd>
					</dl>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="page-header" style="margin: 12px; margin: 12px;">
				<h2>
					Caixas <small>ISCB</small>
				</h2>
				<p align="justify"></p>
			</div>
			<div style="text-align: center">
				<h5 style="color: #0d9617">${msg}</h5>
				<h5 style="color: #e92f32">${error}</h5>
				<h5 style="color: #e92f32">${param.error}</h5>
			</div>
			<table class="table-responsive card-list-table table-striped table-hover">
				<c:choose>
					<c:when test="${not empty entities}">
						<thead>
							<tr>
								<th>#</th>
								<th>Raça</th>
								<th>Machos</th>
								<th>Fêmeas</th>
								<th>Opções</th>
							</tr>
						</thead>
					</c:when>
					<c:when test="${empty entities }">
						<p>Não há registros, adicione um.</p>
					</c:when>
				</c:choose>
				<c:forEach var="caixa" items="${entities}">
					<tr>
						<td data-title="Caixa nº">&ensp;&ensp;${caixa.id }</td>
						<td data-title="Raça">&ensp;&ensp;${caixa.raca }</td>
						<td data-title="Machos">&ensp;&ensp;${caixa.especimesMacho}</td>
						<td data-title="Fêmeas">&ensp;&ensp;${caixa.especimesFemea}</td>
						<td><a class="btn btn-primary btn-sm"
							href="${pageContext.request.contextPath }/caixa/detalhar?id=${caixa.id}"
							data-toggle2="tooltip" data-placement="bottom" title="exibir"><i
								class="fa fa-eye" aria-hidden="true"></i></a>&ensp;<a class="btn btn-info btn-sm"
							href="${pageContext.request.contextPath }/caixa/editar?id=${caixa.id}"
							data-toggle2="tooltip" data-placement="bottom" title="editar"><i
								class="fa fa-pencil-square-o" aria-hidden="true"></i></a>&ensp;<a class="btn btn-danger btn-sm" href="#"
							data-href="remover?id=${caixa.id }" data-toggle="modal"
							data-target="#confirm-delete" data-toggle2="tooltip"
							data-placement="bottom" title="remover"><i
								class="fa fa-trash" aria-hidden="true"></i></a></td>


					</tr>
				</c:forEach>
			</table>
			<br />
			<div class="container-fluid">
				<a class="btn btn-success"
					href="${pageContext.request.contextPath }/caixa/novo?projetoId=${projeto.id}">Novo&ensp;<span
					class="fa fa-plus"></span></a>
			</div>
		</div>
	</div>
	<!-- Modal -->
	<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" style="text-align: center;">Confirmação</h4>
				</div>
				<div class="modal-body" style="text-align: center;">
					<p>A caixa será excluída. Deseja prosseguir?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
					<a class="btn btn-warning btn-ok">Sim</a>
				</div>
			</div>

		</div>
	</div>
	<script>
		$('#confirm-delete').on(
				'show.bs.modal',
				function(e) {
					$(this).find('.btn-ok').attr('href',
							$(e.relatedTarget).data('href'));
				});
		$(document).ready(function() {
			$('[data-toggle2="tooltip"]').tooltip();
		});
	</script>
</body>
</html>
