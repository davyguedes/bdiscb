<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/resources/css/solar.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Erro</title>
</head>
<body>
	<div class="container">
		<h2>Ocorreu um erro.</h2>
		<h4>${errMsg}</h4>
	</div>
</body>
</html>