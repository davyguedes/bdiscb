

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="Equipe Dev IPM">
<link rel="shortcut icon"
	href="${pageContext.request.contextPath }/resources/img/favicon.png">
<!-- Bootstrap core CSS -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/resources/css/bootstrap.min.css">
<link
	href="${pageContext.request.contextPath }/resources/css/signin.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath }/resources/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<title>Área de Login</title>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<link
	href="${pageContext.request.contextPath }/resources/css/ie10-viewport-bug-workaround.css"
	rel="stylesheet">

<!-- Custom styles for this template -->
<link
	href="${pageContext.request.contextPath }/resources/css/signin.css"
	rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script
	src="${pageContext.request.contextPath }/resources/js/ie-emulation-modes-warning.js"></script>
<style type="text/css">
</style>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

	<div class="container">

		<form class="form-signin" action="<c:url value='login' />"
			method='POST'>
			<div align="center">
				<img
					src="${pageContext.request.contextPath }/resources/img/uece_v.png"
					width="80%" alt="">
			</div>
			<div align="center">
				<h3>
					Banco de Dados <strong>ISCB</strong>
				</h3>
			</div>
			<div align="center" class="texto-login2">
				<h4>
					<strong>Área de login</strong>
				</h4>
			</div>

			<label class="sr-only">Login</label> <input type="text"
				name="username" class="form-control" placeholder="Nome Usuário">
			<label for="inputPassword" class="sr-only">Senha</label> <input
				type="password" name="password" class="form-control"
				placeholder="Senha">
			<c:if test="${not empty error}">
				<div class="alert alert-dismissible alert-danger">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<div style="font-style: italic;" align="left">
						<h4>
							<strong>Erro.</strong>
						</h4>${error}</div>
				</div>
			</c:if>
			<%-- 			<h5 style="color: #e92f32">${error}</h5></div> --%>
			<div style="text-align: center">
				<h5 style="color: #0d9617">${msg}</h5>
			</div>
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
			<button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
		</form>
	</div>
	<!-- /container -->
	<div class="row">
		<div class="text-center">
			<h4>Contate-nos:</h4>
			<!-- <a href="http://www.facebook.com/"
				target="_blank"><i class="fa fa-facebook-square fa-3x"
				style="color: #2C3E50"></i></a> <a href=""><i
				class="fa fa-google-plus-square fa-3x" style="color: #DD4B39"></i></a> <a
				href=""><i class="fa fa-envelope-square fa-3x"
				style="color: #1AA095"></i></a>  -->
			<a href="#" data-toggle="tooltip" data-placement="right"
				title="Problemas login: (85) 986286292 Dúvidas: m.davy.br@gmail.com"><i
				class="fa fa-phone-square fa-3x" style="color: #2c3e50"></i></a>
		</div>
		<br> <br>
		<p align="middle">
			<strong>&copy; 2017</strong> Equipe de Desenvolvimento | <strong>LABIEX</strong>
		</p>
	</div>

	<!-- jQuery -->
	<script
		src="${pageContext.request.contextPath }/resources/js/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script
		src="${pageContext.request.contextPath }/resources/js/bootstrap.min.js"></script>



	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script
		src="${pageContext.request.contextPath }/resources/js/ie10-viewport-bug-workaround.js"></script>

	<script>
		$(document).ready(function() {
			$('[data-toggle="tooltip"]').tooltip();
		});
		window.onload = function() {
			document.getElementById("username").focus();
		};
	</script>

</body>
</html>