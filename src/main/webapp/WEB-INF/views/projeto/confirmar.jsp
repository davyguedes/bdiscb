

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet"
	href="${pageContext.request.contextPath }/resources/css/solar.min.css">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Projetos</title>
</head>
<body>
	<div class="container">
		<h2>Confirmação de projeto</h2>
		<form name="newInvestiment" class="form-horizontal"
			action="${pageContext.request.contextPath }/projeto/salvar"
			method="get">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Campo</th>
						<th>Valor</th>
					</tr>
				</thead>

				<tbody>
					<tr>
						<td>Nome do Laboratório</td>
						<td>${projeto.laboratorio}</td>
					</tr>
					<tr>
						<td>Nome do aluno</td>
						<td>${projeto.aluno }</td>
					</tr>
					<tr>
						<td>Número do Comitê de Ética</td>
						<td>${projeto.numeroComiteEtica }</td>
					</tr>
					<tr>
						<td>Projeto</td>
						<td>${projeto.nomeProjeto }</td>
					</tr>
					<tr>
						<td>Descrição do projeto</td>
						<td>${projeto.descricao}</td>
					</tr>

				</tbody>
			</table>
			<div class="container">
				<div class="form-group">

					<button class="btn btn-warning" type="button" name="back"
						onclick="history.back()">Corrigir</button>
					<button type="submit" class="btn btn-success">Confirmar</button>
				</div>
			</div>
		</form>
	</div>
</body>
</html>