<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<link rel="stylesheet"
	href="${pageContext.request.contextPath }/resources/css/bootstrap.min.css">
<%-- <%@ taglib prefix="spring-form" uri="/WEB-INF/tld/spring-form.tld"%> --%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Projetos</title>
</head>
<body>
	<div class="container">
		<h2>Projeto</h2>
		<form class="form-horizontal"
			action="${pageContext.request.contextPath }/projeto/" method="get">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Campo</th>
						<th>Valor</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Nome do Laboratório</td>
						<td>${projeto.laboratorio}</td>
					</tr>
					<tr>
						<td>Nome do aluno</td>
						<td>${projeto.aluno }</td>
					</tr>
					<tr>
						<td>Número do Comitê de Ética</td>
						<td>${projeto.numeroComiteEtica }</td>
					</tr>
					<tr>
						<td>Projeto</td>
						<td>${projeto.nomeProjeto }</td>
					</tr>
					<tr>
						<td>Descrição do projeto</td>
						<td>${projeto.descricao}</td>
					</tr>

				</tbody>
			</table>

			<div class="container">
				<div class="form-group">

					<button class="btn btn-default" type="button" name="back"
						onclick="history.back()">Voltar</button>
					<a class="btn btn-primary"
						href="${pageContext.request.contextPath }/projeto/editar?id=${projeto.id}">Editar</a>
					<a class="btn btn-warning"
						href="${pageContext.request.contextPath }/projeto/remover?id=${projeto.id}">Remover</a>
				</div>
			</div>
		</form>
	</div>
</body>
</html>