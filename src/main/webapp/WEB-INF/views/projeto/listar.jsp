

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/resources/css/solar.min.css">
<link href="${pageContext.request.contextPath }/resources/css/mfb.css"
	rel="stylesheet">
<script src="${pageContext.request.contextPath }/resources/js/mfb.js"></script>
<title>Projetos</title>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="page-header" style="margin: 12px; margin: 12px;">
				<h2>
					Projetos <small>ISCB</small>
				</h2>
				<p align="justify"></p>
			</div>
			<div style="text-align: center">
				<h5 style="color: #0d9617">${msg}</h5>
				<h5 style="color: #e92f32">${error}</h5>
			</div>
			<table class="table-responsive card-list-table">
				<c:choose>
					<c:when test="${not empty entities}">
						<thead>
							<tr>
								<th>Aluno</th>
								<th>Número do Comitê de Ética</th>
								<th>Projeto</th>
								<th>Caixas</th>
								<th colspan="2">Opções</th>
							</tr>
						</thead>
					</c:when>
					<c:when test="${empty entities }">
						<p>Não há registros, adicione um.</p>
					</c:when>
				</c:choose>

				<c:forEach var="projeto" items="${entities}">
					<tr>
						<td data-title="Aluno">&ensp;${projeto.aluno }</td>
						<td data-title="Comite de ética">&ensp;${projeto.numeroComiteEtica }</td>
						<td data-title="Projeto">&ensp;${projeto.nomeProjeto }</td>
						<td data-title="Exibir">&ensp;&ensp;<a class="btn btn-primary btn-sm"
							href="${pageContext.request.contextPath }/caixa/?projetoId=${projeto.id}"><i
								class="fa fa-eye" aria-hidden="true"></i></a></td>
						<td data-title="Editar">&ensp;&ensp;<a class="btn btn-info btn-sm"
							href="${pageContext.request.contextPath }/projeto/editar?id=${projeto.id}"
							data-toggle2="tooltip" data-placement="bottom" title="editar"><i
								class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
						<td data-title="Remover">&ensp;&ensp; <a class="btn btn-danger btn-sm" href="#"
							data-href="remover?id=${projeto.id }" data-toggle="modal"
							data-target="#confirm-delete" data-toggle2="tooltip"
							data-placement="bottom" title="remover"><i
								class="fa fa-trash" aria-hidden="true"></i></a></td>

					</tr>
				</c:forEach>
			</table>
			<br/>
			<div class="container-fluid">
				<a class="btn btn-default"
					href="${pageContext.request.contextPath }/projeto/novo">Novo&ensp;<span
					class="fa fa-plus"></span></a>
			</div>
		</div>
	</div>
	<!-- Modal -->
	<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" style="text-align: center;">Confirmação</h4>
				</div>
				<div class="modal-body" style="text-align: center;">
					<p>O projeto será removido. Deseja prosseguir?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
					<a class="btn btn-warning btn-ok">Sim</a>
				</div>
			</div>

		</div>
	</div>
	<script>
		$('#confirm-delete').on(
				'show.bs.modal',
				function(e) {
					$(this).find('.btn-ok').attr('href',
							$(e.relatedTarget).data('href'));
				});
	</script>
</body>
</html>