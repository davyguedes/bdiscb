

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/resources/css/solar.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Projetos</title>
</head>
<body>
	<div class="container">
		<div class="well bs-component">
			<form:form cssClass="form-horizontal"
				action="${pageContext.request.contextPath }/projeto/confirmar"
				method="post" modelAttribute="entity">
				<fieldset>
					<legend>Projeto</legend>
					<legend></legend>
					<form:errors path="*" cssClass="alert alert-danger" element="div" />
					<input type="hidden" id="id" name="id" value="0" readonly
						class="form-control">

					<div class="form-group">
						<label class="col-lg-2 control-label">Nome do Laboratório</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" name="laboratorio"
								placeholder="nome do laboratório">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">Nome do Aluno</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" name="aluno"
								placeholder="nome do aluno (representante legal)">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">Número do Comitê de
							Ética</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" name="numeroComiteEtica"
								placeholder="número do comitê de ética">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">Projeto</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" name="nomeProjeto"
								placeholder="nome do projeto">
						</div>
					</div>
					<div class="form-group">
						<label for="textArea" class="col-lg-2 control-label">Descrição</label>
						<div class="col-lg-10">
							<textarea class="form-control" rows="3" name="descricao"
								placeholder="descrição..."></textarea>
							<span class="help-block">Uma pequena descrição quanto ao
								projeto, causa, visão e/ou objetivos.</span>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-10 col-lg-offset-2">
							<button type="button" name="back" onclick="history.back()"
								class="btn btn-default">Voltar</button>
							<button type="reset" class="btn btn-warning">Limpar</button>
							<button type="submit" class="btn btn-success">Salvar</button>
						</div>
					</div>
				</fieldset>
			</form:form>
		</div>
	</div>
</body>
</html>